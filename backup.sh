#!/bin/bash

#script extrait de https://linuxhandbook.com/bash-automation/

#répertoires à sauvegarder
backup_dirs=("/etc" "/home" "/boot")

#nom du répertoire de sauvegarde sur serveur distant
dest_dir="/backup"

#serveur sur lequel la sauvegarde sera stocké
dest_server=pc1

#date de sauvegarde
backup_date=$(date +%b-%d-%y)

echo "début de sauvegarde de: ${backup_dirs[@]}..."

for i in "${backup_dirs[@]}"; do

    sudo tar -Pczf /tmp/$i-$backup_date.tar.gz $i
    if [ $? -eq 0 ]; then
        echo "Sauvegarde réussie pour $i "
    else
        echo "Echec de sauvegarde pour $i "
    fi

    #copier le backup sur serveur distant
    scp /tmp/$i-$backup_date.tar.gz $dest_server:$dest_dir

    if [ $? -eq 0 ]; then
        echo "Transfert de $i réussi."
    else
        echo "Transfert de $i échoué."
    fi
done
