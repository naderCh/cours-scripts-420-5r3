#!/bin/bash

#script extrait de https://linuxhandbook.com/bash-automation/

serveurs=$(cat serveurs.txt)

echo -n "Entrer le nom d'utilisateur: "
read name
echo -n "Entrer l'id de l'utilisateur (1001 ou plus): "
read uid

for i in $serveurs; do
    echo $i
    ssh $i "useradd -m -u $uid $name"

    if [ $? -eq 0 ]; then
        echo "Utilisateur $name ajouté sur $i"
    else
        echo "Erreur sur $i"
    fi
done
